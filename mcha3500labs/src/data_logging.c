#include "data_logging.h"
#include <stdint.h>
#include "pendulum.h"

#include <stdint.h>
#include <stdlib.h>
#include <math.h>
#include "stm32f4xx_hal.h"
#include "cmsis_os2.h"
#include "IMU.h"

/* Variable declarations */
uint16_t logCount;

// Timer Declarations
static osTimerId_t logTimerId;
static osTimerAttr_t logTimerAttr =
{
	.name = "logTimer"
};

static void (*log_function)(void);
/* Function declarations */
static void log_pendulum(void);
static void log_imu(void);
static void log_pointer(void *argument);

/* Function defintions */

void logging_init(void)
{
	// // Initialise timer for use with pendulum data logging
	// logTimerId = osTimerNew(log_pendulum, osTimerPeriodic, NULL, &logTimerAttr);

	// TODO: MOdify os timer for use with generic logging function 
	logTimerId = osTimerNew(log_pointer, osTimerPeriodic, NULL, &logTimerAttr);
}

static void log_pointer(void *argument)
{
	UNUSED(argument);

	// Call function pointed to by log_function
	(*log_function)();
}

void pend_logging_start(void)
{

	// TODO: change function pointer to the pendulum logging function
	log_function = &log_pendulum;

	// TODO: Rest the log counter
	logCount = 0;

	// TODO: Start data logging timer at 200 Hz
	osTimerStart(logTimerId, 5);


}


static void log_pendulum(void)
{
 /* TODO: Supress compiler warnings for unused arguments */
	//UNUSED(argument);

 /* TODO: Read the potentiometer voltage */
	float voltage = pendulum_read_voltage();

  //TODO: Print the sample time and potentiometer voltage to the serial terminal in the format [time],[
	float time = logCount*(0.005);
	printf("%f, %f\n", time, voltage);


 /* TODO: Increment log count */
	logCount++;

 /* TODO: Stop logging once 2 seconds is reached (Complete this once you have created the stop function
	in the next step) */
	if (time>=2)
	{
		logging_stop();
	}
}




void logging_stop(void)
{
	// TODO: Stop data logging timer
	osTimerStop(logTimerId);
}

void imu_logging_start(void)
{
	// change function pointer to the imu logging function (log_imu)
	log_function = &log_imu;
	// reset the log counter
	logCount = 0;
	// start data logging at 200 hz
	osTimerStart(logTimerId, 5);
}

static void log_imu(void)
{	

	// TODO: read imu
	IMU_read();

	// TODO: get the imu angle from acceleration readings
	double theta_acc = get_acc_angle();

	// TODO: get the imu X gyro
	float x_gyro = get_gyroX();

	// TODO read the potentiometer voltage
	float voltage_pot = pendulum_read_voltage();

	// TODO Print the time, accelerometer angle, gyron angular velocity and pot voltage values to the serial
	// terminal in the format %f,%f,%f,%f\n
	float time = logCount*(0.005);

	printf("%f,%f,%f,%f\n", time, theta_acc, x_gyro, voltage_pot);
	
	// TODO increment log count
	logCount ++;
	// TODO stop logging once 5 seconds is reached
	if (time>=5)
	{
		logging_stop();
	}
}