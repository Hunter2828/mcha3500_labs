#include <stdint.h>
#include <stdlib.h>
#include <math.h>
#include "stm32f4xx_hal.h"
#include "cmsis_os2.h"
#include "uart.h"
#include "pendulum.h"



ADC_HandleTypeDef hadc1;
ADC_ChannelConfTypeDef sConfigADC;

void pendulum_init(void)
{
	// TODO: Enable ADC1 Clock
	__HAL_RCC_ADC1_CLK_ENABLE();
	// TODO: Enable GPIOB Clock
	__HAL_RCC_GPIOB_CLK_ENABLE();

	/* TODO: Initialise PB0 with:
		- pin 0
		- Analoh mode
		- no pull
		- high frequency */
	GPIO_InitTypeDef  GPIO_InitStruct_PB0;
	GPIO_InitStruct_PB0.Pin = GPIO_PIN_0;
	GPIO_InitStruct_PB0.Mode = GPIO_MODE_ANALOG;
	GPIO_InitStruct_PB0.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOB,&GPIO_InitStruct_PB0);


	/* TODO: Initialise ADC1 with:
		- Instance ADC 1
		- Div 2 prescalar
		- 12 bit resolution
		- data align right
		- continuous conversion mode
		- number of conversion 1 */
	 
	hadc1.Instance = ADC1;
	hadc1.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV2;
	hadc1.Init.Resolution = ADC_RESOLUTION_12B;
	hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
	hadc1.Init.ContinuousConvMode = ENABLE;
	hadc1.Init.NbrOfConversion = 1;
	HAL_ADC_Init(&hadc1);



	 /*
	 NOTE: configfuration parameters not mentioned above are not required for this lab */

	/* TODO: Configure ADC Channel to:
		- Rank 1
		- Channel 8
		- Sampling time 480 cycles
		- offset 0 */
	sConfigADC.Channel = ADC_CHANNEL_8;
	sConfigADC.Rank = 1;
	sConfigADC.SamplingTime = ADC_SAMPLETIME_480CYCLES;
	sConfigADC.Offset = 0;
	HAL_ADC_ConfigChannel(&hadc1,&sConfigADC);

}

float pendulum_read_voltage(void)
{
	/* TODO: Start ADC*/
	HAL_ADC_Start(&hadc1);
	/* TODO: Poll for conversion. Use timeout of 0xFF. */
	HAL_ADC_PollForConversion(&hadc1, 0xFF);
	/* TODO: Get ADC Value */
	static float ADC_pendulum_voltage = 0;
	ADC_pendulum_voltage = HAL_ADC_GetValue(&hadc1);

	/* TODO: Stop ADC*/
	HAL_ADC_Stop(&hadc1);

	/* TODO: Compute voltage from ADC reading. Hint 2^21 - 1 = 4095*/
	static float pendulum_voltage = 0;
	pendulum_voltage = ADC_pendulum_voltage*(3.3/4095);


	/* TODO: Return the computed voltage*/
	return pendulum_voltage;
}