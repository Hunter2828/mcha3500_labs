#include "IMU.h"
#include "tm_stm32_mpu6050.h"
#include <stdint.h>
#include <stdlib.h>
#include "stm32f4xx_hal.h"
#include "cmsis_os2.h"
#include "uart.h"
#include "IMU.h"
#include "math.h"

#ifndef M_PI
	#define M_PI 3.14159265358979323846
#endif




// Variable Declarations
TM_MPU6050_t IMU_datastruct;
 
 // Function definitions
void IMU_init(void)
{
	// TODO: Initialise IMU with AD0 LOW, acceleration sensitivity +-4g, gyroscope +-250 deg/s
	TM_MPU6050_Init(&IMU_datastruct, TM_MPU6050_Device_0, TM_MPU6050_Accelerometer_4G, TM_MPU6050_Gyroscope_250s);
}

void IMU_read(void)
{
	// TODO: Read all IMU values
	TM_MPU6050_ReadAll(&IMU_datastruct);

}

float get_accY(void)
{
	// TODO: Convert acceleration reading to ms^-2
	float Y_accel = IMU_datastruct.Accelerometer_Y/8192.0; // -/+4g = 8192 LSB/g
	float accY = Y_accel*9.81;
	// TODO: return the Y acceleration
	return accY;
}

float get_accZ(void)
{
	// TODO: return the Z acceleration
	float Z_accel = IMU_datastruct.Accelerometer_Z/8192.0; // -/+4g = 8192 LSB/g
	float accZ = Z_accel*9.81;
	// TODO: return the Z acceleration
	return accZ;
}

float get_gyroX(void)
{
	// Return the X angular velocity
	float X_gyro = IMU_datastruct.Gyroscope_X/((32786+32767)/(500.0)); // ((32786+32767)/2.0)/(250)
	X_gyro = X_gyro*M_PI/180.0;
	// TODO: return the Y acceleration
	return X_gyro;
}

double get_acc_angle(void)
{
	// TODO compute IMU angle using accY and accZ using atan2
	float accZ = get_accZ();
	float accY = get_accY();
	double theta_acc = -atan2(accZ, accY);
	// TODO return the IMU angle
	return theta_acc;

}

