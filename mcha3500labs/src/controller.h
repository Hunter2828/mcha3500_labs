#ifndef CONTROLLER_H
#define CONTROLLER_H

// Add function prototypes
void ctrl_init(void);
void ctrl_set_x1(const float x1);
void ctrl_set_x2(const float x2);
void ctrl_set_x3(const float x3);
void ctrl_set_x4(const float x4);
float getControl(void);
void ctrl_update(void);
#endif