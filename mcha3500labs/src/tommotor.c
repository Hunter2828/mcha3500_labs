#include <stdint.h>
#include <stdlib.h>
#include <math.h>
#include "stm32f4xx_hal.h"
#include "cmsis_os2.h"
#include "uart.h"

#include "motor.h"

static TIM_HandleTypeDef _htim3;
static TIM_OC_InitTypeDef _sConfigPWM;
int32_t enc_count = 0;

#define TIM3_PERIOD 10000

void motor_PWM_init(void)
{
    // Enable TIM3 Clock
__HAL_RCC_TIM3_CLK_ENABLE();
    // Enable GPIOA Clock
    __HAL_RCC_GPIOA_CLK_ENABLE();

    /* Initialise PA6 with:
        - Pin 6
        - Alternate function push-pull mode
        - No Pull
        - High Frequency
        - Alternate function 2, Timer 3 */

    GPIO_InitTypeDef  GPIO_InitStructure;
    GPIO_InitStructure.Pin = GPIO_PIN_6;
    GPIO_InitStructure.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStructure.Pull = GPIO_NOPULL;
    GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_HIGH;
    GPIO_InitStructure.Alternate = GPIO_AF2_TIM3;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStructure);

    /* Initialise TIM3 with:
        - Instance TIM3
        - Prescaler of one
        - Counter mode up
        - Timer Period to generate a 10 kHz signal
        - Clock Division of 0 */

    _htim3.Instance = TIM3;
    _htim3.Init.Prescaler = 1;
    _htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
    _htim3.Init.Period = TIM3_PERIOD;
    _htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
    HAL_TIM_PWM_Init(&_htim3);

    /* Configure timer 3 channel 1 with:
        - Output compare mode PWM1
        - Pulse = 0
        - OC Polarity high
        - Fast mode disabled */

    _sConfigPWM.OCMode = TIM_OCMODE_PWM1;
    _sConfigPWM.Pulse = 0;                        
    _sConfigPWM.OCPolarity = TIM_OCPOLARITY_HIGH;
    _sConfigPWM.OCPolarity = TIM_OCFAST_DISABLE;

    HAL_TIM_PWM_ConfigChannel(&_htim3, &_sConfigPWM, TIM_CHANNEL_1);

    // Variable to define duty cycle
    float DutyCycle = 100;

    // Set initial Timer 3, channel 1 compare value
    __HAL_TIM_SET_COMPARE(&_htim3, TIM_CHANNEL_1, (DutyCycle/100.0f)*TIM3_PERIOD);

    // Start Timer 3, Channel 1
    HAL_TIM_PWM_Start(&_htim3, TIM_CHANNEL_1);


}

void motor_encoder_init(void)
{
__HAL_RCC_GPIOC_CLK_ENABLE();

GPIO_InitTypeDef  GPIO_InitStructure;
    GPIO_InitStructure.Pin = GPIO_PIN_0|GPIO_PIN_1;
    GPIO_InitStructure.Mode = GPIO_MODE_IT_RISING_FALLING;
    GPIO_InitStructure.Pull = GPIO_NOPULL;
    GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_HIGH;
    HAL_GPIO_Init(GPIOC, &GPIO_InitStructure);

    HAL_NVIC_SetPriority(EXTI0_IRQn, 0x0F, 0x0F);
    HAL_NVIC_SetPriority(EXTI1_IRQn, 0x0F, 0x0F);

    HAL_NVIC_EnableIRQ(EXTI0_IRQn);
    HAL_NVIC_EnableIRQ(EXTI1_IRQn);

}

void EXTI0_IRQHandler(void)
{
    // Check if PC0 == PC1. Adjust encoder count accordingly
    if (HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_0) == HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_1))
    {
        enc_count++;
    }
    else
    {
        enc_count--;
    }
       
    // Reset Interrupt
HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_0);
}

void EXTI1_IRQHandler(void)
{
    // Check if PC0 == PC1. Adjust encoder count accordingly
    if (HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_0) == HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_1))
    {
        enc_count--;
    }
    else
    {
        enc_count++;
    }

    // Reset Interrupt
HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_1);
}

int32_t motor_encoder_getValue(void)
{
    return enc_count;
}