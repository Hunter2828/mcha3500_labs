#ifndef MOTOR_H
#define MOTOR_H

// Add function protoypes here
void motor_PWM_init(void);
void motor_encoder_init(void);
int32_t motor_encoder_getValue(void);


#endif